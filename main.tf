data "docker_registry_image" "NzbGet" {
	name = "linuxserver/nzbget:latest"
}

resource "docker_image" "NzbGet" {
	name = data.docker_registry_image.NzbGet.name
	pull_triggers = [data.docker_registry_image.NzbGet.sha256_digest]
}

module "NzbGet" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.NzbGet.latest

	networks = [{ name: var.docker_network, aliases: ["nzbget.${var.internal_domain_base}"] }]
  ports = [{ internal: 5055, external: 9610, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/NzbGet"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.downloads}"
			container_path = "/downloads"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}